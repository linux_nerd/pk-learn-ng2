"use strict";

// SystemJS configuration file, see links for more information
// https://github.com/systemjs/systemjs
// https://github.com/systemjs/systemjs/blob/master/docs/config-api.md

/***********************************************************************************************
 * User Configuration.
 **********************************************************************************************/
/** Map relative paths to URLs. */
const map: any = {
  'moment': 'vendor/moment/moment.js',
  '@ng-bootstrap/ng-bootstrap': 'vendor/@ng-bootstrap/ng-bootstrap'
}

const packages: any = {
  'moment':{
    format: 'cjs'
  },
  '@ng-bootstrap/ng-bootstrap': {
    format: 'cjs',
    defaultExtension: 'js',
    main: 'index.js'
  },
  '@ng-bootstrap/ng-bootstrap/accordion': {
    defaultExtension: 'js',
    main: 'index.js'
  },
  '@ng-bootstrap/ng-bootstrap/alert': {
    defaultExtension: 'js',
    main: 'index.js'
  },
  '@ng-bootstrap/ng-bootstrap/buttons': {
    defaultExtension: 'js',
    main: 'index.js'
  },
  '@ng-bootstrap/ng-bootstrap/carousel': {
    defaultExtension: 'js',
    main: 'index.js'
  },
  '@ng-bootstrap/ng-bootstrap/collapse': {
    defaultExtension: 'js',
    main: 'index.js'
  },
  '@ng-bootstrap/ng-bootstrap/dropdown': {
    defaultExtension: 'js',
    main: 'index.js'
  },
  '@ng-bootstrap/ng-bootstrap/pagination': {
    defaultExtension: 'js',
    main: 'index.js'
  },
  '@ng-bootstrap/ng-bootstrap/popover': {
    defaultExtension: 'js',
    main: 'index.js'
  },
  '@ng-bootstrap/ng-bootstrap/progressbar': {
    defaultExtension: 'js',
    main: 'index.js'
  },
  '@ng-bootstrap/ng-bootstrap/rating': {
    defaultExtension: 'js',
    main: 'index.js'
  },
  '@ng-bootstrap/ng-bootstrap/tabset': {
    defaultExtension: 'js',
    main: 'index.js'
  },
  '@ng-bootstrap/ng-bootstrap/timepicker': {
    defaultExtension: 'js',
    main: 'index.js'
  },
  '@ng-bootstrap/ng-bootstrap/tooltip': {
    defaultExtension: 'js',
    main: 'index.js'
  },
  '@ng-bootstrap/ng-bootstrap/typeahead': {
    defaultExtension: 'js',
    main: 'index.js'
  }
};

////////////////////////////////////////////////////////////////////////////////////////////////
/***********************************************************************************************
 * Everything underneath this line is managed by the CLI.
 **********************************************************************************************/
const barrels: string[] = [
  // Angular specific barrels.
  '@angular/core',
  '@angular/common',
  '@angular/compiler',
  '@angular/forms',
  '@angular/http',
  '@angular/router',
  '@angular/platform-browser',
  '@angular/platform-browser-dynamic',

  // Thirdparty barrels.
  'rxjs',

  // App specific barrels.
  'app',
  'app/shared',
  'app/cv-component',
  'app/login',
  'app/cv',
  'app/shared/navigation',
  'app/cv/navigation',
  'app/cv/shared/nav',
  /** @cli-barrel */
];

const cliSystemConfigPackages: any = {};
barrels.forEach((barrelName: string) => {
  cliSystemConfigPackages[barrelName] = { main: 'index' };
});

/** Type declaration for ambient System. */
declare var System: any;

// Apply the CLI SystemJS configuration.
System.config({
  map: {
    '@angular': 'vendor/@angular',
    'rxjs': 'vendor/rxjs',
    'main': 'main.js'
  },
  packages: cliSystemConfigPackages
});

// Apply the user's configuration.
System.config({ map, packages });
