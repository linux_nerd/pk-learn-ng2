import { NgModule }       from '@angular/core';
import { BrowserModule  } from '@angular/platform-browser';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap"

import { routing, appRoutingProviders } from './app.routing';
import { AppComponent }   from './app.component';
import { CvComponent } from "./cv/index";


@NgModule({
    declarations: [AppComponent, CvComponent],
    imports:      [BrowserModule, NgbModule, routing],
    bootstrap:    [AppComponent],
})
export class AppModule {}