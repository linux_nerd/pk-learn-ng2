import { Component, Input } from '@angular/core';
import * as moment from 'moment';


@Component({
  	moduleId: module.id,
  	selector: 'app-root',
  	templateUrl: 'app.component.html',
  	styleUrls: ['app.component.css']
})
export class AppComponent {
	@Input()
	public alert: string = "danger";
  	public title = `app works with moment.js ${moment().format('DD/MMM/YYYY')} !!`;
}
