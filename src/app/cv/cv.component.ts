import { Component, OnInit } from '@angular/core';
import { NavComponent } from "./shared/index";

@Component({
  moduleId: module.id,
  selector: 'app-cv',
  templateUrl: 'cv.component.html',
  styleUrls: ['cv.component.css'],
  directives: [ NavComponent ]
})
export class CvComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
