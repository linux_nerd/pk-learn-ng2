import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule }   from '@angular/router';

import { CvComponent } from "./cv";

const appRoutes: Routes = [
	{ path: "login", loadChildren: 'app/login/login.module' },
	{ path: "", component: CvComponent }
];

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

