import { NgModule }           from '@angular/core';
import {RouterModule} from '@angular/router';

import { LoginComponent }   from './login.component';
import { loginRouting }   from './login.routing';

@NgModule({
  imports:      [ loginRouting ],
  declarations: [ LoginComponent ],
  exports:      [ LoginComponent ]
})
export default class LoginModule {

}
